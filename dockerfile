FROM openjdk:14-alpine

ARG WAR_FILE= /opt/gateway/api-gateway-0.0.1-SNAPSHOT.war

COPY ${WAR_FILE} webapp.war

CMD ["java", "-Dspring.profiles.active=docker", "-jar", "webapp.war"]
